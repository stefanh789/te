<?php

class B{
    $this->a;

    public function __construct(A $a){
        $this->a = $a;
    }
    
    public function doWork(){
        $result = $this->a->calculate(10, 15);

        return $result;
    }
}